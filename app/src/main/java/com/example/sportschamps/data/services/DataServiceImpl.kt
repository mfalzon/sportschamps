package com.example.sportschamps.data.services

import android.util.Log
import com.example.sportschamps.data.api.MemeApi
import com.example.sportschamps.domain.dto.Memes
import io.reactivex.rxjava3.core.Single
import okhttp3.OkHttpClient
import javax.inject.Inject
import javax.inject.Named

class DataServiceImpl @Inject constructor(
    @Named("api") private val okHttpClient: OkHttpClient,
    private val memeApi: MemeApi
) : DataService {

    override fun fetchMemes(): Single<List<Memes>> {
        val a = memeApi.fetchMemeData()
        a.doOnError {
            Log.d("error", it.message ?: "no message"
            ) }

        return a.map {
            it.data.memes
        }
    }

}