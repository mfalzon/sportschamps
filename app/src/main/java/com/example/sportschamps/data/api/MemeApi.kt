package com.example.sportschamps.data.api

import com.example.sportschamps.domain.dto.Data
import com.example.sportschamps.domain.dto.MemeResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface MemeApi {

    @GET("get_memes")
    fun fetchMemeData(): Single<MemeResponse>
}