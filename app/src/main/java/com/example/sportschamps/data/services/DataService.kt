package com.example.sportschamps.data.services

import com.example.sportschamps.domain.dto.Memes
import io.reactivex.rxjava3.core.Single

interface DataService {

    fun fetchMemes(): Single<List<Memes>>
}