package com.example.sportschamps.data.di

import com.example.sportschamps.data.api.MemeApi
import com.example.sportschamps.data.services.DataServiceImpl
import com.example.sportschamps.data.services.DataService
import com.example.sportschamps.domain.JobExecutor
import com.example.sportschamps.domain.PostExecutionThread
import com.example.sportschamps.domain.ThreadExecutor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory

@Module
open class DataModule {

    @Singleton
    @Provides
    @Named("api")
    fun providesHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS) // connect timeout
            .readTimeout(100, TimeUnit.SECONDS)    // socket timeout
            .writeTimeout(100, TimeUnit.SECONDS)
            .addInterceptor(logging)

        return client.build()
    }


    @Singleton
    @Provides
    @Named("retrofit")
    fun providesRetrofit(@Named("gson") gson: Gson, @Named("api") httpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.imgflip.com/")
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient)
            .build()
    }

    @Singleton
    @Provides
    @Named("gson")
    fun providesGson(): Gson {
        return GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .create()
    }

    @Provides
    @Singleton
    fun providesPostExecutionThread(): PostExecutionThread {
        return ExecutionThread
    }

    @Singleton
    @Provides
    fun provideDataService(@Named("api") okHttpClient: OkHttpClient, memeApi: MemeApi
    ): DataService {
        return DataServiceImpl(okHttpClient, memeApi)
    }

    @Singleton
    @Provides
    fun provideThreadExecutor(): ThreadExecutor {
        return JobExecutor
    }

    object ExecutionThread : PostExecutionThread {
        override val scheduler: Scheduler
            get() = AndroidSchedulers.mainThread()
    }

    @Singleton
    @Provides
    fun providesMemeApi(@Named("retrofit") retrofit: Retrofit): MemeApi {
        return retrofit.create<MemeApi>(MemeApi::class.java)
    }

}