package com.example.sportschamps.testclasses

import io.reactivex.rxjava3.android.plugins.RxAndroidPlugins
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import io.reactivex.rxjava3.schedulers.Schedulers

object SpekHelpers {

    fun rxBeforeTests() {
        val trampoline = Schedulers.trampoline()

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { trampoline }
        RxJavaPlugins.setIoSchedulerHandler { trampoline }
        RxJavaPlugins.setNewThreadSchedulerHandler { trampoline }
        RxJavaPlugins.setComputationSchedulerHandler { trampoline }
    }

    fun rxAfterTests() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}
