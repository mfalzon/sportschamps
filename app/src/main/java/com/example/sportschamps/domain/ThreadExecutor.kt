package com.example.sportschamps.domain

import io.reactivex.rxjava3.core.Scheduler
import java.util.concurrent.Executor

interface ThreadExecutor : Executor {
    val scheduler: Scheduler
}