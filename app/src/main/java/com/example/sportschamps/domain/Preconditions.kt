package com.example.sportschamps.domain

object Preconditions {

    fun <T> checkNotNull(reference: T?): T {
        return reference ?: throw NullPointerException()
    }

    fun checkNotDisposed(useCase: BaseAbstractUseCase) {
        if(useCase.allDisposed()){
            throw IllegalStateException("Use case $useCase has been disposed and should not be re-used.")
        }
    }
}