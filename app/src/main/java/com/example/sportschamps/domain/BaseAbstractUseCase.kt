package com.example.sportschamps.domain

import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

abstract class BaseAbstractUseCase {

    private var disposables: CompositeDisposable = CompositeDisposable()

    /**
     * Dispose from current [CompositeDisposable].
     */
    fun dispose() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }

    fun clear() {
        if (!disposables.isDisposed) {
            disposables.clear()
        }
    }

    /**
     * Used for test validation
     */
    fun allDisposed(): Boolean {
        return disposables.isDisposed
    }

    protected fun addDisposable(disposable: Disposable) {
        Preconditions.checkNotNull(disposable)
        Preconditions.checkNotNull(disposables)
        disposables.add(disposable)
    }

    class MissingParamsException : IllegalArgumentException("Params are missing from call")
}