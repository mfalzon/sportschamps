package com.example.sportschamps.domain

import com.example.sportschamps.domain.model.Meme
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

interface UseCase<TInput, TOutput> {
    fun execute(params: TInput): Single<List<Meme>>
}

/**
 * An encapsulation of business logic enforcing a common input/output as an flowable stream
 */
interface FlowableUseCase<TInput, TOutput> : UseCase<TInput, Flowable<TOutput>>

/**
 * An encapsulation of business logic enforcing a common input/output as a Single response stream
 */
interface SingleUseCase<TInput, TOutput> : UseCase<TInput, Single<TOutput>>

/**
 * An encapsulation of business logic enforcing a common input/output as a Maybe response stream
 */
interface MaybeUseCase<TInput, TOutput> : UseCase<TInput, Maybe<TOutput>>

/**
 * An encapsulation of business logic enforcing a common input as a Completable stream
 */
interface CompletableUseCase<TInput> : UseCase<TInput, Completable>
