package com.example.sportschamps.domain

import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

object JobExecutor : ThreadExecutor {

    override val scheduler: Scheduler
        get() = Schedulers.from(this)

    private val threadPoolExecutor: ThreadPoolExecutor = createExecutor()

    private fun createExecutor() = ThreadPoolExecutor(3, 5, 10, TimeUnit.SECONDS,
        LinkedBlockingQueue(), JobThreadFactory())

    override fun execute(runnable: Runnable) {
        this.threadPoolExecutor.execute(runnable)
    }

    private class JobThreadFactory : ThreadFactory {
        private var counter = 0

        override fun newThread(runnable: Runnable): Thread {
            return Thread(runnable, "android_" + counter++)
        }
    }
}