package com.example.sportschamps.domain

import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.Consumer
import io.reactivex.rxjava3.observers.DisposableSingleObserver


/**
 * Abstract class for a Use Case (Interactor in terms of Clean Architecture).
 * This interface represents a execution unit for different use cases (this means any use case
 * in the application should implement this contract).
 * By convention each AbstractSingleUseCase implementation will return the result using a [DisposableSingleObserver]
 * that will execute its job in a background thread and will post the result in the UI thread.
 */
abstract class AbstractSingleUseCase<T, in Params> internal constructor(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) : BaseAbstractUseCase() {

    /**
     * Builds an [Single] which will be used when executing the current [AbstractObservableUseCase].
     */
    abstract fun build(params: Params? = null): Single<T>

    /**
     * Executes the current use case.

     * @param observer [DisposableSingleObserver] which will be listening to the observable build
     * by [.buildUseCaseObservable] ()} method.
     *
     * @param params Parameters (Optional) used to build/execute this use case.
     */
    open fun execute(observer: DisposableSingleObserver<T>, params: Params) {
        execute(params, observer::onSuccess, observer::onError)
    }

    /**
     * Executes the current use case.

     * @param observer [DisposableSingleObserver] which will be listening to the observable build
     * by [.buildUseCaseObservable] ()} method.
     *
     */
    open fun execute(observer: DisposableSingleObserver<T>) {
        execute(observer::onSuccess, observer::onError)
    }

    /**
     * Executes the current use case.
     * @param params Parameters used to build/execute this use case.
     * @param onSuccess method invoked on success
     * @param onError method invoked on failure
     */
    fun execute(params: Params, onSuccess: (T) -> Unit, onError: (Throwable) -> Unit) {
        execute(onSuccess, onError, params)
    }

    /**
     * Executes the current use case.
     * @param onSuccess method invoked on success
     * @param onError method invoked on failure
     */
    fun execute(onSuccess: (T) -> Unit, onError: (Throwable) -> Unit) {
        execute(onSuccess, onError, null)
    }

    /**
     * Executes the current use case.
     * @param params Parameters (Optional) used to build/execute this use case.
     * @param onSuccess method invoked on success
     * @param onError method invoked on failure
     */
    private fun execute(onSuccess: (T) -> Unit, onError: (Throwable) -> Unit, params: Params?) {
        val single = params?.let { build(params) } ?: build()
        addDisposable(single
            .subscribeOn(threadExecutor.scheduler)
            .observeOn(postExecutionThread.scheduler)
            .subscribe(
                Consumer(onSuccess),
                Consumer { err -> if (!allDisposed()) onError(err) }
            ))
    }
}
