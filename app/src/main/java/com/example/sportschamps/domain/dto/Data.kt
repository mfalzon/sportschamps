package com.example.sportschamps.domain.dto

import com.google.gson.annotations.SerializedName

data class Data (

    @SerializedName("memes") val memes : List<Memes>
)

data class MemeResponse (

    @SerializedName("success") val success : Boolean,
    @SerializedName("data") val data : Data
)