package com.example.sportschamps.domain.meme

import com.example.sportschamps.domain.model.Meme
import com.example.sportschamps.data.services.DataService
import com.example.sportschamps.domain.*
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

class FetchMemeUseCase(
    private val dataService: DataService,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : AbstractSingleUseCase<List<Meme>, Unit>(threadExecutor, postExecutionThread) {

     fun execute(params: Unit): Single<List<Meme>> {
        return dataService.fetchMemes().map { it ->
            it.map{
                Meme(it.id, it.name)
            }
        }
    }

    override fun build(params: Unit?): Single<List<Meme>> {
        return dataService.fetchMemes().map { it ->
            it.map{
                Meme(it.id, it.name)
            }
        }
    }


}