package com.example.sportschamps.domain.model

import android.os.Parcel
import android.os.Parcelable


data class Meme(
    val id: Int,
    val name: String,
    var checked: Boolean = false
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Meme> {
        override fun createFromParcel(parcel: Parcel): Meme {
            return Meme(parcel)
        }

        override fun newArray(size: Int): Array<Meme?> {
            return arrayOfNulls(size)
        }
    }
}