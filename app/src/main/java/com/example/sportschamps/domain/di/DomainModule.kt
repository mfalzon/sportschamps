package com.example.sportschamps.domain.di

import com.example.sportschamps.domain.meme.FetchMemeUseCase
import com.example.sportschamps.data.services.DataService
import com.example.sportschamps.domain.PostExecutionThread
import com.example.sportschamps.domain.ThreadExecutor
import dagger.Module
import dagger.Provides

@Module
object DomainModule {
    @Provides
    fun provideFetchMemeUseCase(dataRepository: DataService, threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread
    ): FetchMemeUseCase = FetchMemeUseCase(dataRepository, threadExecutor, postExecutionThread)

}