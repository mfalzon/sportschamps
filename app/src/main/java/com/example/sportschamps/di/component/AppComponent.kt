package com.example.sportschamps.di.component

import com.example.sportschamps.SportsChampsApplication
import com.example.sportschamps.data.di.DataModule
import com.example.sportschamps.di.module.ActivityBuildersModule
import com.example.sportschamps.di.module.FragmentBuildersModule
import com.example.sportschamps.domain.di.DomainModule
import com.example.sportschamps.presentation.fragment.meme.di.MemeFragmentBinderModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuildersModule::class,
    DomainModule::class,
    DataModule::class,
    //FragmentBuildersModule::class,
    MemeFragmentBinderModule::class
])
interface AppComponent : AndroidInjector<SportsChampsApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: SportsChampsApplication): Builder

        fun build(): AppComponent
    }

}