package com.example.sportschamps.di.module

import com.example.sportschamps.presentation.activity.main.MainActivity
import com.example.sportschamps.presentation.activity.main.di.MainBinderModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(modules = [MainBinderModule::class])
    abstract fun contributesMainActivity(): MainActivity
}