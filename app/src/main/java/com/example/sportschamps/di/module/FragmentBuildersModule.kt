package com.example.sportschamps.di.module

import com.example.sportschamps.presentation.fragment.meme.MemeFragment
import com.example.sportschamps.presentation.fragment.meme.di.MemeFragmentBinderModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    //@ContributesAndroidInjector(modules = [MemeFragmentBinderModule::class])
    //abstract fun contributesMemeFragment(): MemeFragment
}