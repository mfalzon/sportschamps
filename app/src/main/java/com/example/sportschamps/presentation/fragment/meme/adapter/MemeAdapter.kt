package com.example.sportschamps.presentation.fragment.meme.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sportschamps.R
import com.example.sportschamps.domain.model.Meme
import kotlinx.android.synthetic.main.list_meme.view.*


class MemeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{

     var items: List<Meme> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MemeViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_meme, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {

            is MemeViewHolder -> {
                holder.bind(items[position])
            }

        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addData(memeList: List<Meme>){
        val oldItems = items.toList()
        items = memeList
        if(oldItems.isNotEmpty())
        items = oldItems.map {
            it.copy(checked = it.checked)
        }
        notifyDataSetChanged()
    }

    class MemeViewHolder
    constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView){

        private val memeId: TextView = itemView.tv_id
        private val memeName = itemView.tv_name
        private val checkBox = itemView.cb_checkBox

        fun bind(meme: Meme){

            memeId.text = meme.id.toString()
            memeName.text = meme.name
            checkBox.isChecked = meme.checked

            checkBox.setOnClickListener {
                meme.checked = checkBox.isChecked
            }

        }

    }

}