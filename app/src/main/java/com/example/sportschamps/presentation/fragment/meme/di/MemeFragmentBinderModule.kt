package com.example.sportschamps.presentation.fragment.meme.di

import androidx.lifecycle.ViewModel
import com.example.sportschamps.di.ViewModelKey
import com.example.sportschamps.presentation.fragment.meme.MemeFragment
import com.example.sportschamps.presentation.fragment.meme.MemeViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MemeFragmentBinderModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMemeFragment(): MemeFragment

    @Binds
    @IntoMap
    @ViewModelKey(MemeViewModel::class)
    internal abstract fun bindMemeFragmentViewModel(MemeViewModel: MemeViewModel): ViewModel
}
