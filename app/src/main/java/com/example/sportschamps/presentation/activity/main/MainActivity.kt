package com.example.sportschamps.presentation.activity.main

import android.os.Bundle
import androidx.activity.viewModels
import com.example.sportschamps.R
import com.example.sportschamps.presentation.activity.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<MainViewModel>() {

    private val TAG = "MainActivity"

    override val viewModel: MainViewModel by viewModels { viewModelProviderFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
    }
}