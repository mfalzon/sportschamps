package com.example.sportschamps.presentation.activity.main


import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.LiveData
import com.example.sportschamps.domain.meme.FetchMemeUseCase
import com.example.sportschamps.domain.model.Meme
import com.example.sportschamps.presentation.activity.BaseViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val fetchMemeUseCase: FetchMemeUseCase
): BaseViewModel() {

}