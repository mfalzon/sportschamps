package com.example.sportschamps.presentation.fragment.meme

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sportschamps.domain.meme.FetchMemeUseCase
import com.example.sportschamps.domain.model.Meme
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MemeViewModel @Inject constructor(
    private val fetchMemeUseCase: FetchMemeUseCase
): ViewModel() {

    private val memeData: MutableLiveData<List<Meme>> = MutableLiveData()
    fun getData(): MutableLiveData<List<Meme>> = memeData

    fun fetchData() {
        return fetchMemeUseCase.execute(Unit,
        onSuccess = {
                    memeData.postValue(it)
        },
        onError = {
            memeData.postValue(null)
        })
    }
}