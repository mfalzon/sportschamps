package com.example.sportschamps.presentation.fragment.meme

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sportschamps.R
import com.example.sportschamps.di.ViewModelProviderFactory
import com.example.sportschamps.domain.model.Meme
import com.example.sportschamps.presentation.fragment.meme.adapter.MemeAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_meme.*

import javax.inject.Inject
import android.os.Parcelable
import android.util.Log
import androidx.lifecycle.Observer


class MemeFragment: DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory

   private lateinit var memeAdapter: MemeAdapter

    private val layoutId = R.layout.fragment_meme
    val viewModel: MemeViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onStart() {
        super.onStart()
        initRecyclerView()
        initObservers()

    }

    private fun initRecyclerView() {
        list_meme.apply {
            layoutManager = LinearLayoutManager(requireContext())
            memeAdapter = MemeAdapter()
            adapter = memeAdapter
        }
    }

    override fun onStop() {
        super.onStop()
        viewModel.getData().postValue(memeAdapter.items)
    }

    private fun initObservers(){
        viewModel.getData().observe(this, {
            if(it != null){
                memeAdapter.addData(it)
                startPostponedEnterTransition()
            } else {
                Toast.makeText(requireContext(), "Cannot find memes", Toast.LENGTH_LONG).show()
            }
        })
        if(viewModel.getData().value != null){
            viewModel.getData().value?.let {
                memeAdapter.addData(it)
                startPostponedEnterTransition()
            }
        } else {
            viewModel.fetchData()
        }
    }
}