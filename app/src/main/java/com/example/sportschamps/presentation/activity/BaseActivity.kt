package com.example.sportschamps.presentation.activity

import android.os.Bundle
import com.example.sportschamps.di.ViewModelProviderFactory
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity<V : BaseViewModel>: DaggerAppCompatActivity() {

    abstract val viewModel: V

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProviderFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}