package com.example.sportschamps.presentation.activity.main.di

import androidx.lifecycle.ViewModel
import com.example.sportschamps.di.ViewModelKey
import com.example.sportschamps.presentation.activity.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainBinderModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}