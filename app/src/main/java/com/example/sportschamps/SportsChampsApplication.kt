package com.example.sportschamps

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import com.example.sportschamps.di.component.DaggerAppComponent

class SportsChampsApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>?
            = DaggerAppComponent.builder().application(getApplicationModule()).build()

    private fun getApplicationModule() = this

    override fun onCreate() {
        super.onCreate()
    }

}