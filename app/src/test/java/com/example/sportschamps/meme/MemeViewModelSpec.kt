package com.example.sportschamps.meme

import androidx.lifecycle.Observer
import com.example.sportschamps.PresentationTestHelpers
import com.example.sportschamps.domain.meme.FetchMemeUseCase
import com.example.sportschamps.mother.MemeDataMother
import com.example.sportschamps.presentation.fragment.meme.MemeViewModel
import com.example.sportschamps.testclasses.SpekHelpers
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.reset
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.util.*

class MemeViewModelSpec : Spek({

    val fetchMemeUseCase: FetchMemeUseCase = mock()

    val impl by memoized {
        MemeViewModel(fetchMemeUseCase)
    }

    beforeGroup {
        SpekHelpers.rxBeforeTests()
        PresentationTestHelpers.liveDataBeforeTests()
    }

    afterGroup {
        SpekHelpers.rxAfterTests()
        PresentationTestHelpers.liveDataAfterTests()
    }

    afterEachTest {
        reset(
            fetchMemeUseCase
        )
    }

    describe("Fetch data"){
        val observer: Observer<Boolean> = mock()

        it("should fetch data"){
            //whenever(fetchMemeUseCase.execute(Unit)).thenReturn(Flowable.just(MemeDataMother.create()))
           // impl.fetchData().obse
        }
    }

}) {
}