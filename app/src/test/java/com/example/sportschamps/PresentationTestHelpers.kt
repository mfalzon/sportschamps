package com.example.sportschamps

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor

object PresentationTestHelpers {
    fun liveDataBeforeTests() {
        ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
            override fun executeOnDiskIO(runnable: Runnable) {
                runnable.run()
            }

            override fun isMainThread(): Boolean {
                return true
            }

            override fun postToMainThread(runnable: Runnable) {
                runnable.run()
            }
        })
    }

    fun liveDataAfterTests() {
        ArchTaskExecutor.getInstance().setDelegate(null)
    }
}

