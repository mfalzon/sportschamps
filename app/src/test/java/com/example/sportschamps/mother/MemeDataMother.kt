package com.example.sportschamps.mother

import com.example.sportschamps.domain.model.Meme

object MemeDataMother {

    fun create(): List<Meme> {
        return mutableListOf(Meme(1, "test1"), Meme(2, "test2"), Meme(3, "test3"))

    }

}